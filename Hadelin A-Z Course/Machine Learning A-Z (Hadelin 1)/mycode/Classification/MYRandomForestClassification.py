#RandomForest Classifier
#Decision Tree clasification

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#import dataset
dataset = pd.read_csv('Social_Network_Ads.csv')
#identify independent and dependent variable
X=dataset.iloc[:,2:-1].values
y=dataset.iloc[:,-1].values

'''from sklearn.preprocessing import Imputer
imputer=Imputer(missing_values='NaN',strategy='mean',axis=0)
imputer=imputer.fit(X[:,1:3])
X[:,1:3]=imputer.transform(X[:,1:3])

#Encoding categorical data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X=LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
onehotencoder=OneHotEncoder(categorical_features=[0])
X=onehotencoder.fit_transform(X).toarray()
lebelencoder_y=LabelEncoder()
y=labelencoder_X.fit_transform(y)'''

#Splitting the dataset into the Training set and Test set

#When we use a small sample you dont need to split model
from sklearn.cross_validation import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X,y, test_size=0.25, random_state=0)


#Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X=StandardScaler()
Xtrain=sc_X.fit_transform(Xtrain)
Xtest=sc_X.transform(Xtest)


#Create our clasifier
#Random Forest Classification Model to the dataset
from sklearn.ensemble import RandomForestClassifier
classifier=RandomForestClassifier(n_estimators=10,criterion='entropy',random_state=0)
classifier.fit(Xtrain,Ytrain)
#Predict a new result from regression to test set
y_pred=classifier.predict(Xtest)

#compare 2 matrix Making confusion matrix
from sklearn.metrics import confusion_matrix
cm=confusion_matrix(Ytest,y_pred)


#Visualize the training set result
from matplotlib.colors import ListedColormap
Xset,Yset=Xtrain,Ytrain
X1,X2= np.meshgrid(np.arange(start=Xset[:,0].min()-1,stop=Xset[:,0].max()+1, step=0.01),
                   np.arange(start=Xset[:,1].min()-1,stop=Xset[:,1].max()+1,step=0.01))
plt.contourf(X1 , X2 , classifier.predict(np.array([X1.ravel(),X2.ravel()]).T).reshape(X1.shape),
             alpha=0.75,cmap=ListedColormap(('red','green'))) #double bracket for color is needed
plt.xlim(X1.min(),X1.max())
plt.ylim(X2.min(),X2.max())
for i,j in enumerate(np.unique(Yset)):
    plt.scatter(Xset[Yset==j,0],Xset[Yset==j,1],
                c=ListedColormap(('red','green'))(i),label=j)
plt.title('Random forest classifier (Training set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()
#Visualize test-set result
from matplotlib.colors import ListedColormap
Xset,Yset=Xtest,Ytest
X1,X2= np.meshgrid(np.arange(start=Xset[:,0].min()-1,stop=Xset[:,0].max()+1, step=0.01),
                   np.arange(start=Xset[:,1].min()-1,stop=Xset[:,1].max()+1,step=0.01))
plt.contourf(X1 , X2 , classifier.predict(np.array([X1.ravel(),X2.ravel()]).T).reshape(X1.shape),
             alpha=0.75,cmap=ListedColormap(('red','green'))) #double bracket for color is needed
plt.xlim(X1.min(),X1.max())
plt.ylim(X2.min(),X2.max())
for i,j in enumerate(np.unique(Yset)):
    plt.scatter(Xset[Yset==j,0],Xset[Yset==j,1],
                c=ListedColormap(('red','green'))(i),label=j)
plt.title('Random forest classifier (Test set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()


























































