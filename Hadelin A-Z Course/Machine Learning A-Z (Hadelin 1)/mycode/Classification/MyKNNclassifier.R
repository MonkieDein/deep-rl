# KNN  K-Nearest Neighbor

#Import the dataset
setwd("C:/Users/USER/Desktop/Machine Learning A-Z Template Folder/Part 3 - Classification/Section 14 - Logistic Regression/Logistic_Regression")
dataset=read.csv(('Social_Network_Ads.csv'))
dataset=dataset[,3:5]

# Encoding the target feature as factor

#Taking care of missing data
# dataset$Age=ifelse(is.na(dataset$Age),
#                    ave(dataset$Age, FUN =function(x) mean(x, na.rm=TRUE)),
#                    dataset$Age)
# 
# dataset$Salary=ifelse(is.na(dataset$Salary),
#                       ave(dataset$Salary, FUN =function(x) mean(x, na.rm=TRUE)),
#                       dataset$Salary)

#Encoding categorical data
# dataset$Country=factor((dataset$Country),
#                        levels=c('France','Spain','Germany'),
#                        labels=c(1,2,3))
# dataset$Purchased=factor((dataset$Purchased),
#                        levels=c('No','Yes'),
#                        labels=c(0,1))

#splitting the dataset into the training set and test set
#install.packages('caTools')
library(caTools)
set.seed(123) #set a fix random method so that answer is the same
split=sample.split(dataset$Purchased, SplitRatio = 0.75)
trainingset=subset(dataset, split==TRUE)
testset=subset(dataset, split==FALSE)

#Feature scaling
trainingset[,1:2]=scale(trainingset[,1:2])
testset[,1:2]=scale(testset[,1:2])

#Fitting KNN Regression to the Training Set
# create classifier

#Fitting KNN Regression to the Training Set
library(class)
y_pred = knn(train=trainingset[,-3],
             test=testset[,-3],
             cl=trainingset[,3],
             k=5)

#make a confusion matrix
cm=table(testset[,3],y_pred)

#Visualizing the training set
#install.packages('ElemStatLearn')
library(ElemStatLearn)
set=trainingset
x1=seq(min(set[,1])-1,max(set[,1])+1,by=0.01)
x2=seq(min(set[,2])-1,max(set[,2])+1,by=0.01)
grid_set=expand.grid(x1,x2)
colnames(grid_set)=c('Age','EstimatedSalary')
ygrid=knn(train=trainingset[,-3],
          test=grid_set,
          cl=trainingset[,3],
          k=5)
plot(set[,-3],
     main='KNN Regression(Trainingset)',
     xlab='Age',ylab='Estimated Salary',
     xlim=range(x1),ylim=range(x2))

contour(x1, x2, matrix(as.numeric(ygrid), length(x1), length(x2)), add = TRUE)
points(grid_set,pch='.',col=ifelse(ygrid==1,'springgreen3','tomato'))
points(set,pch=21,bg=ifelse(set[,3]==1,'green4','red3'))


#Visualizing the test set
#install.packages('ElemStatLearn')

set=testset
x1=seq(min(set[,1])-1,max(set[,1])+1,by=0.01)
x2=seq(min(set[,2])-1,max(set[,2])+1,by=0.01)
grid_set=expand.grid(x1,x2)
colnames(grid_set)=c('Age','EstimatedSalary')
ygrid=knn(train=trainingset[,-3],
          test=grid_set,
          cl=trainingset[,3],
          k=5)
plot(set[,-3],
     main='Logistic Regression(Testset)',
     xlab='Age',ylab='Estimated Salary',
     xlim=range(x1),ylim=range(x2))

contour(x1, x2, matrix(as.numeric(ygrid), length(x1), length(x2)), add = TRUE)
points(grid_set,pch='.',col=ifelse(ygrid==1,'springgreen3','tomato'))
points(set,pch=21,bg=ifelse(set[,3]==1,'green4','red3'))
