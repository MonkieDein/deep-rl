#Data preprocessing template
#import library that is number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#import dataset
dataset = pd.read_csv('Data.csv')
#identify independent and dependent variable
X=dataset.iloc[:,:-1].values
y=dataset.iloc[:,-1].values

from sklearn.preprocessing import Imputer
imputer=Imputer(missing_values='NaN',strategy='mean',axis=0)
X=imputer.fit_transform(X[:,1:])
'''
#Encoding categorical data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X=LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
onehotencoder=OneHotEncoder(categorical_features=[0])
X=onehotencoder.fit_transform(X).toarray()
lebelencoder_y=LabelEncoder()
y=labelencoder_X.fit_transform(y)

'''
# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X,y, test_size=(1/3), random_state=0)

#Feature Scaling
'''from sklearn.preprocessing import StandardScaler
sc_X=StandardScaler()
Xtrain=sc_X.fit_transform(Xtrain)
Xtest=sc_X.transform(Xtest)'''

#Fitting simple Linear Regression to the training set
from sklearn.linear_model import LinearRegression
regressor= LinearRegression()
regressor.fit(Xtrain,Ytrain)

#Predict the test set result
Ypred=regressor.predict(Xtest)

#Visualize the training set result
plt.scatter(Xtrain, Ytrain,color='red')
plt.plot(Xtrain,regressor.predict(Xtrain),color='blue')
plt.title('Salary vs Experience(Training set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()

#Visualize the test set
plt.scatter(Xtest, Ytest,color='red')
plt.plot(Xtrain,regressor.predict(Xtrain),color='blue')
plt.title('Salary vs Experience(Training set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()














