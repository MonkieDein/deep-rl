#Data Preprocessing

#Importing the dataset
setwd("C:/Users/USER/Desktop/Machine Learning A-Z Template Folder/Part 2 - Regression/Section 5 - Multiple Linear Regression/Multiple_Linear_Regression")
dataset= read.csv('50_Startups.csv')
#dataset=dataset[,2:3]

#Taking care of missing data
# dataset$Age=ifelse(is.na(dataset$Age),
#                    ave(dataset$Age, FUN =function(x) mean(x, na.rm=TRUE)),
#                    dataset$Age)
# 
# dataset$Salary=ifelse(is.na(dataset$Salary),
#                       ave(dataset$Salary, FUN =function(x) mean(x, na.rm=TRUE)),
#                       dataset$Salary)

#Encoding categorical data
dataset$State=factor((dataset$State),
                       levels=c('New York','California','Florida'),
                       labels=c(1,2,3))

#splitting the dataset into the training set and test set
#install.packages('caTools')
library(caTools)
set.seed(123) #set a fix random method so that answer is the same
split=sample.split(dataset$Profit, SplitRatio = 0.8)
trainingset=subset(dataset, split==TRUE)
testset=subset(dataset, split==FALSE)

#Feature scaling 
# training_set[,2:3]=scale(training_set[,2:3])
# test_set[,2:3]=scale(test_set[,2:3])

#Fitting Polynomial Regression to the Training Set
X_big=seq(1,10,length.out = 1001)

multi_regressor = lm(formula = Profit ~ R.D.Spend,
                     data=trainingset)
summary(multi_regressor)

#Predict the y with testset
ypred=predict(multi_regressor,newdata = testset)








