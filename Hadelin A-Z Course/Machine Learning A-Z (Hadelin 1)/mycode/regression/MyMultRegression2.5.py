#Data preprocessing template
#import library that is number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#import dataset
dataset = pd.read_csv('50_Startups.csv')
#identify independent and dependent variable
X=dataset.iloc[:,:-1].values
y=dataset.iloc[:,-1].values

'''from sklearn.preprocessing import Imputer
imputer=Imputer(missing_values='NaN',strategy='mean',axis=0)
imputer=imputer.fit(X[:,1:3])
X[:,1:3]=imputer.transform(X[:,1:3])
'''

#Encoding categorical data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X=LabelEncoder()
X[:,3]=labelencoder_X.fit_transform(X[:,3])
onehotencoder=OneHotEncoder(categorical_features=[3])
X=onehotencoder.fit_transform(X).toarray()

#avoiding dummy trap
X=X[:,1:]
'''
lebelencoder_y=LabelEncoder()
y=labelencoder_X.fit_transform(y)
'''

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X,y, test_size=0.2, random_state=0)

#Feature Scaling
'''from sklearn.preprocessing import StandardScaler
sc_X=StandardScaler()
Xtrain=sc_X.fit_transform(Xtrain)
Xtest=sc_X.transform(Xtest)'''

# Fitting Multi Linear regression to the training set
from sklearn.linear_model import LinearRegression
regressor =LinearRegression()
regressor.fit(Xtrain,Ytrain)

#Predicting the test set result
y_pred = regressor.predict(Xtest)

#Building the optimal model using backward Elimination
import statsmodels.formula.api as sm
#since this library does not indlude the constant
X=np.append(arr=np.ones((50,1)).astype(int),values=X,axis=1)
X_opt=X[:,[0,1,2,3,4,5]]
#fit ordinary least square
regressor_OLS=sm.OLS(endog=y,exog=X_opt).fit()
regressor_OLS.summary()

#Since we are using back ward elimination we wanna eliminate the one with highest p-value
#Until all the p value is smaller than 0.05
#fit ordinary least square
X_opt=X[:,[0,3]]
regressor_OLS=sm.OLS(endog=y,exog=X_opt).fit()
regressor_OLS.summary()






