#Data Preprocessing

#Importing the dataset
setwd("C:/Users/USER/Desktop/Machine Learning A-Z Template Folder/Part 2 - Regression/Section 6 - Polynomial Regression/Polynomial_Regression")
dataset= read.csv('Position_Salaries.csv')
dataset=dataset[,2:3]

#Taking care of missing data
# dataset$Age=ifelse(is.na(dataset$Age),
#                    ave(dataset$Age, FUN =function(x) mean(x, na.rm=TRUE)),
#                    dataset$Age)
# 
# dataset$Salary=ifelse(is.na(dataset$Salary),
#                       ave(dataset$Salary, FUN =function(x) mean(x, na.rm=TRUE)),
#                       dataset$Salary)

#Encoding categorical data
# dataset$State=factor((dataset$State),
#                        levels=c('New York','California','Florida'),
#                        labels=c(1,2,3))

#splitting the dataset into the training set and test set
#install.packages('caTools')
# library(caTools)
# set.seed(123) #set a fix random method so that answer is the same
# split=sample.split(dataset$Profit, SplitRatio = 0.8)
# trainingset=subset(dataset, split==TRUE)
# testset=subset(dataset, split==FALSE)

#Feature scaling 
# training_set[,2:3]=scale(training_set[,2:3])
# test_set[,2:3]=scale(test_set[,2:3])

#Fitting Polynomial Regression to the Training Set
dataset$level2=dataset$Level^2 #Make a polynomial term
dataset$level3=dataset$Level^3
dataset$level4=dataset$Level^4
X_big=seq(1,10,length.out = 1001)

lin_regressor= lm(formula = Salary ~ Level,
              data=dataset)
summary(lin_regressor)

poly_regressor= lm(formula = Salary ~ .,
              data=dataset)
summary(poly_regressor)
#Predict the y with testset
ypred_lin=predict(lin_regressor,newdata = dataset)
ypred_poly=predict(poly_regressor,data.frame(Level=X_big,
                                             level2=X_big^2,
                                             level3=X_big^3,
                                             level4=X_big^4))

#visualizing the lienaer result
#install.packages('ggplot2')
library(ggplot2)         
ggplot()+
  geom_point(aes(x = dataset$Level, y = dataset$Salary),
             colour='red')+
  geom_line(aes(x=dataset$Level, y = ypred_lin),
            colour='blue')+
  ggtitle('Level vs Salary(Linear)')+
  xlab('Level')+
  ylab('Salary')
#visualize the polynomial result
library(ggplot2)         
ggplot()+
  geom_point(aes(x = dataset$Level, y = dataset$Salary),
             colour='red')+
  geom_line(aes(x=X_big, y = ypred_poly),
            colour='blue')+
  ggtitle('Level vs Salary(Poly4)')+
  xlab('Level')+
  ylab('Salary')

#predict a point
lin_pred=predict(lin_regressor,data.frame(Level=6.5))
poly_pred=predict(poly_regressor,data.frame(Level=6.5,
                                           level2=6.5^2,
                                           level3=6.5^3,
                                           level4=6.5^4))







