# Apriori

# Data Preprocessing
setwd("C:/Users/USER/Desktop/Machine Learning A-Z Template Folder/Part 5 - Association Rule Learning/Section 28 - Apriori/Apriori")
dataset=read.csv('Market_Basket_Optimisation.csv',header=FALSE)
#Does not take all all receipt it take all the product and see who buy it and what they buy it with

#install.packages('arules')
library(arules)
dataset=read.transactions('Market_Basket_Optimisation.csv',sep=',',rm.duplicates=TRUE)
summary(dataset)
itemFrequencyPlot(dataset,topN=100)

#Training Apriori on the dataset
rules=apriori(data=dataset, parameter=list(support=0.004, confidence=0.2))
#support = expected sale/total sale 3*7/7500
inspect(sort(rules,by='lift')[1:10])



