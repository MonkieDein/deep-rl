# Apriori (Association)
#Data preprocessing template
#import library that is number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#import dataset
dataset = pd.read_csv('Market_Basket_Optimisation.csv',header=None)
#identify independent and dependent variable
transactions=[]
for i in range(0,len(dataset)):
    transactions.append([str(dataset.values[i,j]) for j in range(0,20)])

#Training Apriori on the dataset
from apyori import apriori
rules = apriori( transactions , min_support = 0.003, min_confidence = 0.2 , min_lift = 3 , min_length = 2 )
#Lets say this is a week transaction. We expect the support is bought 3 times a day out of 7 days==21/7501
#Confidence should not be too high it might find items that people just buy in general but not related
def inspect(results):
    rh          = [tuple(x[2][0][0])[0] for x in results]
    lh          = [tuple(x[2][0][1])[0] for x in results]
    supports    = [x[1] for x in results]
    confidences = [x[2][0][2] for x in results]
    lifts       = [x[2][0][3] for x in results]
    return list(zip(rh,lh,supports,confidences,lifts))

#Visualizing the result
results = list(rules)
myResults = [list(x) for x in results]

#the line creates a data frame which
viewable_results=pd.DataFrame(inspect(results))









