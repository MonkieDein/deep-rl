#Thomson Sampling 

#Data preprocessing template
#import library that is number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
#read csv
dataset=pd.read_csv('Ads_CTR_Optimisation.csv')

#Implementing Thompson Sampling
d=len(dataset.transpose())
n=len(dataset)

ads_selected=[]
numbers_of_rewards_1=[0]*d
numbers_of_rewards_0=[0]*d
total_reward=0

for n in range(0,n):
    ad=0
    max_random=0
    
    for i in range(0,d):
        random_beta=random.betavariate(numbers_of_rewards_1[i]+1,numbers_of_rewards_0[i]+1)
        if random_beta> max_random:
            max_random=random_beta
            ad=i
    ads_selected.append(ad)
    reward=dataset.values[n,ad]
    print(reward)
    numbers_of_rewards_1[ad]=numbers_of_rewards_1[ad]+reward
    numbers_of_rewards_0[ad]=numbers_of_rewards_0[ad]+1-reward
    total_reward=total_reward+reward
    
    
#Visualizing the effect
plt.hist(ads_selected)
plt.title('Histogram of ads selections')
plt.xlabel('Ads')
plt.ylabel('Number of time each ad was selected')
plt.show
