#Convolutional Neutral Network

#Installing Theano
#pip install --upgrade --no deps git+git://hithub.com/Theano/Theano.git

#Installing Tensorflow
# Install Tensorflow from the websites: https://www.tensorflow.org/versions/r0.12/get_started/

#Installing Keras
#pip install --upgrade keras

#Part 1 Building the Convolutional Neuro Network

#Importing the Keraas Libraries and packages
import tensorflow as tf
import keras

from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense


#Initialising the CNN
classifier = Sequential()

#Step1-Convolution
classifier.add(Convolution2D(32,3,3,input_shape=(64,64,3),activation='relu'))

#Step2-Pooling
classifier.add(MaxPooling2D(pool_size=(2,2)))

#2nd convolution layer
#Step1-Convolution
classifier.add(Convolution2D(32,3,3,activation='relu'))

#Step2-Pooling
classifier.add(MaxPooling2D(pool_size=(2,2)))

#Flattening 
classifier.add(Flatten())

#Adding the input layer and the first hidden layer
classifier.add(Dense(output_dim=128,activation='relu'))
classifier.add(Dense(output_dim=1,activation='sigmoid')) 
#softmax(x, axis=-1) for multivariate output

#Compiling the CNN
classifier.compile(optimizer='adam',loss='binary_crossentropy', metrics=['accuracy'])


#Part2 - Fitting the CNN to the training set
from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

train_set = train_datagen.flow_from_directory(
        'dataset/training_set',
        target_size=(64,64),
        batch_size=32,
        class_mode='binary')

test_set = test_datagen.flow_from_directory(
        'dataset/test_set',
        target_size=(64, 64),
        batch_size=32,
        class_mode='binary')

classifier.fit_generator(
        train_set,
        steps_per_epoch=3,
        epochs=15,
        validation_data=test_set,
        validation_steps=2000)






