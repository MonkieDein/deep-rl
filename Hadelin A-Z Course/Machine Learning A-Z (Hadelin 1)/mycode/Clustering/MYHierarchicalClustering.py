#Hierarchical Clustering

#import library
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

dataset=pd.read_csv("Mall_Customers.csv")
X=dataset.iloc[:,3:].values

#plot dendrogram
import scipy.cluster.hierarchy as sch
dendrogram=sch.dendrogram(sch.linkage(X, method='ward'))
plt.xlabel('Customers')
plt.ylabel('Euclidean Distance')
plt.title('Dendrogram')
plt.show()

#Fitting hierarchical clustering to the mall dataset
from sklearn.cluster import AgglomerativeClustering
hc=AgglomerativeClustering (n_clusters=5,affinity='euclidean',linkage='ward')
y_hc=hc.fit_predict(X)

#Visualizing clusters
plt.scatter(X[y_hc==0,0],X[y_hc==0,1],s=100,color='red')
plt.scatter(X[y_hc==1,0],X[y_hc==1,1],s=100,color='orange')
plt.scatter(X[y_hc==2,0],X[y_hc==2,1],s=100,color='yellow')
plt.scatter(X[y_hc==3,0],X[y_hc==3,1],s=100,color='green')
plt.scatter(X[y_hc==4,0],X[y_hc==4,1],s=100,color='blue')
plt.title('Cluster of customers')
plt.xlabel('Annual Income (k$)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()


