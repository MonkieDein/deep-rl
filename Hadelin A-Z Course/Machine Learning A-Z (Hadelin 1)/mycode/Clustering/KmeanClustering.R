#KMean

#Import the dataset
setwd("C:/Users/USER/Desktop/Machine Learning A-Z Template Folder/Part 4 - Clustering/Section 24 - K-Means Clustering/K_Means")

dataset=read.csv(('Mall_Customers.csv'))
X=dataset[,4:5]

#Taking care of missing data
# dataset$Age=ifelse(is.na(dataset$Age),
#                    ave(dataset$Age, FUN =function(x) mean(x, na.rm=TRUE)),
#                    dataset$Age)
# 
# dataset$Salary=ifelse(is.na(dataset$Salary),
#                       ave(dataset$Salary, FUN =function(x) mean(x, na.rm=TRUE)),
#                       dataset$Salary)

#Encoding categorical data
# dataset$Country=factor((dataset$Country),
#                        levels=c('France','Spain','Germany'),
#                        labels=c(1,2,3))
# dataset$Purchased=factor((dataset$Purchased),
#                        levels=c('No','Yes'),
#                        labels=c(0,1))

#splitting the dataset into the training set and test set
#install.packages('caTools')
library(caTools)
set.seed(6) #set a fix random method so that answer is the same
#Using albow method
wcss=vector()
for (i in 1:10) wcss[i] = sum(kmeans(X,i)$withinss)

#Apply K-means to the mall dataset
set.seed(29)
kmeans=kmeans(X,5,iter.max =300,nstart=10)

# Visualising the clusters
library(cluster)
clusplot(X,
         kmeans$cluster,
         lines=0,
         shade=TRUE,
         color=TRUE,
         labels=2,
         plotchar=FALSE,
         span=TRUE,
         main=paste('Clusters of clients'),
         xlab="Annual Income",
         ylab="Spending Score")





