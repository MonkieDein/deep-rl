#Heirachical Clustering

#Import the dataset
setwd("C:/Users/USER/Desktop/Machine Learning A-Z Template Folder/Part 4 - Clustering/Section 24 - K-Means Clustering/K_Means")

dataset=read.csv(('Mall_Customers.csv'))
X=dataset[,4:5]

#Taking care of missing data
# dataset$Age=ifelse(is.na(dataset$Age),
#                    ave(dataset$Age, FUN =function(x) mean(x, na.rm=TRUE)),
#                    dataset$Age)
# 
# dataset$Salary=ifelse(is.na(dataset$Salary),
#                       ave(dataset$Salary, FUN =function(x) mean(x, na.rm=TRUE)),
#                       dataset$Salary)

#Encoding categorical data
# dataset$Country=factor((dataset$Country),
#                        levels=c('France','Spain','Germany'),
#                        labels=c(1,2,3))
# dataset$Purchased=factor((dataset$Purchased),
#                        levels=c('No','Yes'),
#                        labels=c(0,1))

#Using the dendrogram to find the optimal number of cluster
dendrogram= hclust(dist(X,method='euclidean'),method='ward.D')
plot(dendrogram,
     main=paste('Dendrogram'),
     xlab='Customers',
     ylab='Euclidean Distances')
#From the longest line look horrizontal and find the number of line

# Fitting the hierarchical clustering to the mall dataset
hc = hclust(dist(X,method='euclidean'),method='ward.D')
y_hc=cutree(hc,5)

# Visualising the clusters
library(cluster)
clusplot(X,
         y_hc,
         lines=0,
         shade=TRUE,
         color=TRUE,
         labels=2,
         plotchar=FALSE,
         span=TRUE,
         main=paste('Clusters of clients'),
         xlab="Annual Income",
         ylab="Spending Score")

