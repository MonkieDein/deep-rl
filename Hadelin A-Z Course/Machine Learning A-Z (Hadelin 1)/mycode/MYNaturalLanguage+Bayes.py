#Natural Language Processing

#Importing the library
import numpy as np
import matplotlib as plt
import pandas as pd

import re
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
#nltk.download('all')
#Importing the dataset
dataset=pd.read_csv('Restaurant_Reviews.tsv', delimiter='\t',quoting=3)
corpus=[]
#Cleaning the text
for i in range(0,len(dataset)):
    review=re.sub('[^a-zA-Z]',' ' ,dataset['Review'][i]) #keep all the a to z and get rid of symbol and number
    review=review.lower() #change all character to lower case
    review=review.split() #split word from space
    ps=PorterStemmer() #unify all tenses to normal tense
    review=[ps.stem(word) for word in review if not word in set(stopwords.words('english'))] #take away words that is useless
    review=' '.join(review)
    corpus.append(review)

#Identify dependen variable
y=dataset['Liked']
y=y.reshape(len(y),1)

#Creating the Bag of Word Model
from sklearn.feature_extraction.text import CountVectorizer
cv=CountVectorizer(max_features=1500) #an easier way to make a sparse matrix
X = cv.fit_transform(corpus).toarray()

#Mostly used model is Naive base and Random Forest
from sklearn.cross_validation import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X,y, test_size=0.20, random_state=0)


#Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X=StandardScaler()
Xtrain=sc_X.fit_transform(Xtrain)
Xtest=sc_X.transform(Xtest)

#Create our clasifier
#Fitting Regression Model to the dataset
from sklearn.naive_bayes import GaussianNB
classifier=GaussianNB()
classifier.fit(Xtrain,Ytrain)

#Predict a new result from regression to test set
y_pred=classifier.predict(Xtest)

#compare 2 matrix Making confusion matrix
from sklearn.metrics import confusion_matrix
cm=confusion_matrix(Ytest,y_pred)
accuracy=(cm[1,1]+cm[0,0])/len(Ytest)



