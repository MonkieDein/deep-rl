#Artificial Neural Network

# Installing Theano
# pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git

# Installing Tensorflow
# Install Tensorflow from the website: https://www.tensorflow.org/versions/r0.12/get_started/os_setup.html

# Installing Keras
# pip install --upgrade keras

# Part 1 -Data Preprocessing
#Import Library
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


#import dataset
dataset = pd.read_csv('Churn_Modelling.csv')
#identify independent and dependent variable
X=dataset.iloc[:,3:-1].values
y=(dataset.iloc[:,-1].values).reshape(len(dataset),1)

'''from sklearn.preprocessing import Imputer
imputer=Imputer(missing_values='NaN',strategy='mean',axis=0)
imputer=imputer.fit(X[:,1:3])
X[:,1:3]=imputer.transform(X[:,1:3])
'''
#Encoding categorical data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X1=LabelEncoder()
X[:,1]=labelencoder_X1.fit_transform(X[:,1])
labelencoder_X2=LabelEncoder()
X[:,2]=labelencoder_X2.fit_transform(X[:,2])
onehotencoder=OneHotEncoder(categorical_features=[1])
X=onehotencoder.fit_transform(X).toarray()
X=X[:,1:]
#lebelencoder_y=LabelEncoder()
#y=labelencoder_y.fit_transform(y)

#Splitting the dataset into the Training set and Test set

#When we use a small sample you dont need to split model
from sklearn.model_selection import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X,y, test_size=0.2, random_state=0)


#Feature Scaling
#from sklearn.preprocessing import StandardScaler
#sc_X=StandardScaler()
#Xtrain=sc_X.fit_transform(Xtrain)
#Xtest=sc_X.transform(Xtest)

# Part 2 - Now Let's make the ANN!

#Importing the Keras Libraries and Packages
import theano
import tensorflow as tf
import keras
from keras.layers import Dense
from keras.models import Sequential


#Initialising the ANN
classifier=Sequential()

#Addinf the input layer and the first hidden layer
classifier.add(Dense(output_dim=6,init='uniform',activation='relu',input_dim=11))

# Adding the input layer and the first hidden layer 
classifier.add(Dense(output_dim=6,init='uniform',activation='relu'))

#Adding the output layer
classifier.add(Dense(output_dim=1,init='uniform',activation='sigmoid')) #softmax(x, axis=-1) for multivariate output

#Compiling the ANN
classifier.compile(optimizer='adam',loss='binary_crossentropy', metrics=['accuracy'])

#Fitting the ANN to the training set
classifier.fit(Xtrain,Ytrain, batch_size=10, epochs=100)


from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

# Part 3 - Making a prediction


#Predict a new result from regression to test set
y_pred=classifier.predict(Xtest)
y_pred=(y_pred>0.5)
#compare 2 matrix Making confusion matrix
from sklearn.metrics import confusion_matrix
cm=confusion_matrix(Ytest,y_pred)
accuracy=(cm[1,1]+cm[0,0])/len(Ytest)


