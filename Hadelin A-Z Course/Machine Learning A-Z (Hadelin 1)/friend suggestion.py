# Apriori (Association)
#Data preprocessing template
#import library that is number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import apyori
from random import shuffle

def inspect(results):
    rh          = [tuple(x[2][0][0])[0] for x in results]
    lh          = [tuple(x[2][0][1])[0] for x in results]
    supports    = [x[1] for x in results]
    confidences = [x[2][0][2] for x in results]
    lifts       = [x[2][0][3] for x in results]
    return list(zip(rh,lh,supports,confidences,lifts))

#Make a random dataset
n=40 # number of users
vec=np.random.randint(n/2, size=n)
x = [i for i in range(1,n+1)]
shuffle(x)
friendlist=np.zeros((n,int(n/2)))
for j in range(n):
    shuffle(x)
    friendlist[j][:(vec[j])]=x[:(vec[j])]
friendlist=friendlist-1
friendlist=friendlist.astype(int).astype(str)
friendlist[friendlist=='-1']='nan'
mutualfriend = friendlist.tolist()
#mutualfriend 
#mutualfriend[mutualfriend[i for i in range(1,n+1)==0]]='nan'
##import dataset
#dataset = pd.read_csv('Market_Basket_Optimisation.csv',header=None)

#identify independent and dependent variable
#mutualfriends=[]
#for i in range(0,len(dataset)):
#    mutualfriends.append([str(dataset.values[i,j]) for j in range(0,20)])

#Training Apriori on the dataset
from apyori import apriori
rules = apriori( mutualfriend , min_support = 0.15, min_confidence = 0.9 , min_lift = 3 , min_length = 2 )
#Lets say this is a week transaction. We expect the support is bought 3 times a day out of 7 days==21/7501
#Confidence should not be too high it might find items that people just buy in general but not related

#Visualizing the result
results = list(rules)
resulter=results[1]

#the line creates a data frame which
viewable_results=pd.DataFrame(inspect(results))








