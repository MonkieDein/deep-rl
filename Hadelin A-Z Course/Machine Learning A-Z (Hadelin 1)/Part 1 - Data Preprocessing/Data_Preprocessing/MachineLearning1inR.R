#Data Preprocessing

#Importing the dataset
setwd("C:/Users/USER/Desktop/Machine Learning A-Z Template Folder/Part 1 - Data Preprocessing/Data_Preprocessing")
dataset= read.csv('Data.csv')

# dataset=dataset[,2:3]

#TAKING CARE OF MISSING DATA

# dataset$Age[is.na(dataset$Age)]=mean(dataset$Age,na.rm=TRUE)
# dataset$Salary[is.na(dataset$Salary)]=mean(dataset$Salary,na.rm = TRUE)


#Encoding categorical data

# dataset$Country=factor(dataset$Country,
#                        levels=c('France','Spain','Germany'),
#                        labels=c(1,2,3))
# 
# dataset$Purchased=factor(dataset$Purchased,
#                        levels=c('No','Yes'),
#                        labels=c(0,1))

#splitting the dataset into the training set and test set
#install.packages('caTools')
library(caTools)
set.seed(123)
split=sample.split(dataset$Purchased, SplitRatio = 0.8)
trainingset=subset(dataset,split==TRUE)
testset=subset(dataset,split==FALSE)

#Feature Scaling

# trainingset[,2:3]=scale(trainingset[,2:3])
# testset[,2:3]=scale(testset[,2:3])

