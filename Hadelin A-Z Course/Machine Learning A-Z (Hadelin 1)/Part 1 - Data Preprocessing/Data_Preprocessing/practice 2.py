# -*- coding: utf-8 -*-
"""
Created on Fri Oct 13 17:30:14 2017

@author: USER
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 17:12:39 2017

@author: USER
"""
#import library that is number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#import dataset
dataset = pd.read_csv('Data.csv')
#identify independent and dependent variable
X=dataset.iloc[:,:-1].values
y=dataset.iloc[:,3].values


from sklearn.preprocessing import Imputer
imputer=Imputer(missing_values='NaN',strategy='mean',axis=0)
imputer=imputer.fit(X[:,1:3])
X[:,1:3]=imputer.transform(X[:,1:3])

#Encoding categorical data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X=LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
onehotencoder=OneHotEncoder(categorical_features=[0])
X=onehotencoder.fit_transform(X).toarray()
lebelencoder_y=LabelEncoder()
y=lebelencoder_y.fit_transform(y)

#pd.DataFrame(X)
#X.fillna()
# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X,y, test_size=0.2, random_state=0)