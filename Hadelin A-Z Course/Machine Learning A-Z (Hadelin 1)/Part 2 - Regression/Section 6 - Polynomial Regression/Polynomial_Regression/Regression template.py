#Regression Template
#Data preprocessing template
#import library that is number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#import dataset
dataset = pd.read_csv('Position_Salaries.csv')
#identify independent and dependent variable
X=dataset.iloc[:,:-1].values
y=dataset.iloc[:,-1].values

'''from sklearn.preprocessing import Imputer
imputer=Imputer(missing_values='NaN',strategy='mean',axis=0)
imputer=imputer.fit(X[:,1:3])
X[:,1:3]=imputer.transform(X[:,1:3])

#Encoding categorical data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X=LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
onehotencoder=OneHotEncoder(categorical_features=[0])
X=onehotencoder.fit_transform(X).toarray()
lebelencoder_y=LabelEncoder()
y=labelencoder_X.fit_transform(y)'''

#Splitting the dataset into the Training set and Test set
'''
#When we use a small sample you dont need to split model
from sklearn.cross_validation import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X,y, test_size=0.2, random_state=0)
'''

#Feature Scaling
'''from sklearn.preprocessing import StandardScaler
sc_X=StandardScaler()
Xtrain=sc_X.fit_transform(Xtrain)
Xtest=sc_X.transform(Xtest)'''

#Fitting Regression Model to the dataset

#Predict a new result from Polynomial regression
y_pred=regressor.predict((6.5))

#let X_matrix have a min of 0 max of 10 and addition of 0.01 per time
#Turn X into a matrix instead of an array
X_matrix=np.arange(0,10,0.01)
X_matrix=X_matrix.reshape(len(X_matrix),1)

#Semi perfect Fit but not prefered
#X_matrix just used in plot so that the plot is HD
#Visualize the polynomial regression result
plt.scatter(X, y,color='red')
#plt.plot(X,poly_lin_reg.predict(X_poly),color='blue') certain case
plt.plot(X_matrix,regressor.predict(X_matrix),color='blue')
ply.title('Truth or Bluff (Regression Model)')
plt.xlabel('Level')
plt.ylabel('Salary')
plt.show()


















