# Sept 19th 2018 

# Importing Library

import numpy as np
import random
import os
import torch
import torch.nn as nn 
import torch.nn.functional as F
import torch.optim as optim # Optimization with Stochastic Gradient Descend
import torch.autograd as autograd
from torch.autograd import Variable

# Creating the architecture of the Neural Network

# Inheriting from parent class
class Network(nn.Module):
    def __init__(self, input_size, nb_action):
        super(Network,self).__init__() # Function in PyTorch
        self.input_size = input_size # Define the variable in our class
        self.nb_action = nb_action # Define the variable in our class
        self.fc1 = nn.Linear(input_size,40) # Full connection (input, hidden)
        self.fcmid = nn.Linear(40,40) # Intermediate Layer
        self.fc2 = nn.Linear(40,nb_action) # Full connection2 (hidden, output action)

# Forward Propagation
    def forward(self,state):
        x = F.relu(self.fc1(state))
        y = F.relu(self.fcmid(x))
        q_values = self.fc2(y)
        return q_values
    
# Implementing Experience Replay
        
class ReplayMemory(object):
    
    def __init__(self,capacity):
        self.capacity = capacity
        self.memory = []
        
    def push(self, event):
        self.memory.append(event)
        if len(self.memory) > self.capacity:
            del self.memory[0]
            
    def sample(self, batch_size):
        # if list = ((1,2,3),(4,5,6)), then zip(*list) = ((1,4),(2,5),(3,6))
        samples = zip(*random.sample(self.memory, batch_size))
        return map(lambda x: Variable(torch.cat(x,0)),samples) 
        
# Implementing Deep Q Learning

class Dqn():

    def __init__(self, input_size, nb_action, gamma):
        self.gamma = gamma
        self.reward_window = []
        self.model = Network(input_size, nb_action)        
        self.memory = ReplayMemory(100000)
        self.optimizor = optim.Adam(self.model.parameters(),lr = 0.001) #RMS is good a lot optimizor are good
        self.last_state = torch.Tensor(input_size).unsqueeze(0)
        self.last_action = 0
        self.last_reward = 0
        
    def select_action(self, state):
        probs = F.softmax(self.model(Variable(state, volatile = True))*100) # T = 7, a higher T result a better choice from distribution
        # softmax([1,2,3]) = [0.04,0.11,0.85] => SOFTMAX([1,2,3]* <T> ) = (0,0.02,0.98)
        action = probs.multinomial()
        return action.data[0,0] #Index(1,2)
    
    def learn(self, batch_state, batch_next_state, batch_reward, batch_action):
        outputs = self.model(batch_state).gather(1,batch_action.unsqueeze(1)).squeeze(1)
        next_outputs = self.model(batch_next_state).detach().max(1)[0]
        target = self.gamma*next_outputs +batch_reward
        td_loss = F.smooth_l1_loss(outputs,target) # loss function
        self.optimizor.zero_grad() # Gradient descend , zero_gred reinitialize optimisor every loop
        td_loss.backward(retain_variables = True) # Retain_variable = TRUE help back propagation
        self.optimizor.step() # Update the weight
        
    def update(self, reward, new_signal): # Update the element when observe a new state
        new_state = torch.Tensor(new_signal).float().unsqueeze(0) # Convert new signal into torch tensor
        self.memory.push((self.last_state, new_state, torch.LongTensor([int(self.last_action)]), torch.Tensor([self.last_reward]))) # State(t),State(t+1),Action(t),Reward(t)
        action = self.select_action(new_state)
        if len(self.memory.memory) > 100:
            batch_state, batch_next_state, batch_action, batch_reward = self.memory.sample(100)
            self.learn(batch_state, batch_next_state, batch_reward, batch_action)
        self.last_action = action
        self.last_state = new_state
        self.last_reward = reward
        self.reward_window.append(reward)
        if len(self.reward_window) > 1000:
            del self.reward_window[0]
        return action
    
    def score(self):
        return sum(self.reward_window)/(len(self.reward_window)+1.)
        
    def save(self):
        torch.save({'state_dict':self.model.state_dict(),
                    'optimizor': self.optimizor.state_dict(),
                    }, 'my_last_brain.pth')
    
    def load(self):
        if os.path.isfile('my_last_brain.pth'):
            print("=> loading checkpoint...")
            checkpoint = torch.load('my_last_brain.pth')
            self.model.load_state_dict(checkpoint['state_dict'])
            self.optimizor.load_state_dict(checkpoint['optimizor'])
            print("done !")
        else:
            print("no checkpoint found...")
        
# Test AI
# 1.Round Trip Airport Downtown
# 2.Follow Road
# 3.Make Some harder Road
# 4.Make very complicated road
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        



