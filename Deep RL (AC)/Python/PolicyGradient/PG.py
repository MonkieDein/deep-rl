# ----------------------------------------------
#          Install Library
# ----------------------------------------------
#                   In anaconda prompt
# python -m pip install –upgrade pip
# pip install torch 
# pip install gym 
# pip install numpy 
# pip install pybullet
# pip install utils 
# conda install swig # needed to build Box2D in the pip install
# pip install box2d-py # a repackaged version of pybox2d


# ----------------------------------------------
#          Import Library
# ----------------------------------------------

import torch as T
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import gym
# from utils import plotLearning
import pybullet_envs
from gym import wrappers

# ----------------------------------------------
#          Create Neural Network
# ----------------------------------------------
class NN(nn.Module):
  def __init__(self, lr, input_dims, fc1_dims, fc2_dims, n_actions):
    super(NN, self).__init__()

    self.input_dims = input_dims
    self.lr = lr
    self.fc1_dims = fc1_dims
    self.fc2_dims = fc2_dims 
    self.n_actions = n_actions
    # * unpack tuple or list
    self.fc1 = nn.Linear(*self.input_dims, self.fc1_dims)
    self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
    self.fc3 = nn.Linear(self.fc2_dims, self.n_actions)
    self.optimizer = optim.Adam(self.parameters(), lr=lr)
    # Use GPU if available
    self.device = T.device("cuda" if T.cuda.is_available() else "cpu")
    self.to(self.device)

  def forward(self, observation):
    state = T.Tensor(observation).to(self.device)
    # Input --Relu-> fc1_dims --Relu-> fc2_dims --Linear-> actions
    x = F.relu(self.fc1(state))
    x = F.relu(self.fc2(x))
    x = self.fc3(x) 
    return x

# ----------------------------------------------
#          Create Agent
# ----------------------------------------------

class Agent(object):
  def __init__(self, lr, input_dims, gamma = 0.99, n_actions = 4,
               l1_size = 256, l2_size = 256):
    self.gamma = gamma
    self.reward_memory = []
    self.action_memory = []
    self.policy = NN(lr, input_dims, l1_size, l2_size, n_actions)

  def choose_action(self, observation):
    probs = F.softmax(self.policy.forward(observation))
    action_probs = T.distributions.Categorical(probs)
    action = action_probs.sample()
    log_probs = action_probs.log_prob(action)
    self.action_memory.append(log_probs)

    return action.item()

  def store_rewards(self, reward):
    self.reward_memory.append(reward)

  def learn(self):
    self.policy.optimizer.zero_grad()
    
    G = np.zeros_like(self.reward_memory, dtype=np.float64)
    for t in range(len(self.reward_memory)):
      G_sum = 0
      discount = 1 # declare initial discount
      for k in range(t, len(self.reward_memory)):
        G_sum += self.reward_memory[k] * discount
        discount *= self.gamma
      G[t] = G_sum

    # Normalize
    mean = np.mean(G)
    std = np.std(G) if np.std(G) > 0 else 1
    G = (G-mean)/std
    # Turn into tensor type
    G = T.tensor(G,dtype = T.float).to(self.policy.device)
    
    loss = 0
    for g, logprob in zip(G,self.action_memory):
      loss += -g * logprob

    # perform backprop
    loss.backward()
    self.policy.optimizer.step()
    # reset rewards and action buffer
    self.action_memory = []
    self.reward_memory = []

# ----------------------------------------------
#          Main Program
# ----------------------------------------------

if __name__ == '__main__':
    env = gym.make('LunarLander-v2')
    agent = Agent(lr = 0.001, input_dims=[8], gamma=0.99, n_actions=4,
                l1_size = 128,l2_size=128)
    score_history = []
    score = 0
    n_episodes = 5000

    # env = wrappers.Monitor(env, ' tmp/lunar-lander', force =True)
    for i in range(n_episodes):
        if (i%100==0):
            print('episode: ',i,'score %.3f' %score )
        done = False
        score = 0
        observation = env.reset()
        while not done:
            action = agent.choose_action(observation)
            observation_, reward, done, info = env.step(action)
            agent.store_rewards(reward)
            observation = observation_
            score += reward
        score_history.append(score)
        agent.learn()
    
    with open('lunar-landerPG.txt', 'w') as f:
        for item in score_history:
            f.write("%s\n" %item)
        # plotLearning(score_history, filename='lunar-lander.png', window=25)
        
# with open('lunar-landerPG.txt','rb') as filehandle:
#   score_history = pickle.load(filehandle)
